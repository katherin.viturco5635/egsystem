<main>
    <!--? Slider Area Start-->
    <div class="slider-area">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height hero-overly d-flex align-items-center">
                <div class="container">
                    <div class="row d-flex align-items-center">
                        <div class="col-xl-8 col-lg-6 col-md-8 ">
                            <div class="hero__caption">
                                <span data-animation="fadeInLeft" data-delay=".3s">january 23</span>
                                <h1 data-animation="fadeInLeft" data-delay=".5s" data-duration="2000ms">Anita<strong></strong> Ar</h1>
                                <p data-animation="fadeInLeft" data-delay=".9s">Information Systems Engineering</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Slider Area End-->
